# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
# Path to your local folder with all your local git repositories 
export PROJECTS="$HOME/Repos"
# Path to your local dockerfile library for your cli images
export CLI_DOCKERFILE_DIR="$PROJECTS/dot-files/cli-sandbox"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

zsh_reload () {
	source $HOME/.zshrc
	source $ZSH
}

# Automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"
source $ZSH/oh-my-zsh.sh

# How often to auto-update (in days).
export UPDATE_ZSH_DAYS=14

# Uncomment the following line to enable command auto-correction.
export ENABLE_CORRECTION="true"

# Display red dots whilst waiting for completion.
export COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git docker cp docker-compose helm kubectl python sublime zsh_reload zsh-interactive-cd)

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nano'
else
  export EDITOR='nano'
fi

# Source the helper shortcuts 
source $HOME/.zshrc_helper
# https://github.com/junegunn/fzf
[ -f $HOME/.fzf.zsh ] && source $HOME/.fzf.zsh
